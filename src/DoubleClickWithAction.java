import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class DoubleClickWithAction {

	public static WebDriver driver;
	public static Actions action;
	public static Alert alert;

	@BeforeTest

	public void setup() {
		System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver");
		driver = new FirefoxDriver();
		action = new Actions(driver);
		driver.get("http://artoftesting.com/sampleSiteForSelenium.html");
		driver.manage().window().maximize();
	}

	@Test
	public void performDoubleClick() throws InterruptedException {
		WebElement element = driver.findElement(By.id("dblClkBtn"));
		action.doubleClick(element).build().perform();
		Thread.sleep(500);
		alert = driver.switchTo().alert();
		String actualAlertText = alert.getText();
		System.out.println("Actual alert text : " + actualAlertText);
		alert.accept();
		Thread.sleep(500);
	}

	@AfterTest
	public void cleanup() {
		driver.quit();
	}

}
