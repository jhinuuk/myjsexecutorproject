import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class JSEXecutorClass1 {

	public static WebDriver driver;
	public static JavascriptExecutor js;
	public static final String uname = "Admin";
	public static final String pass = "admin123";

	@BeforeTest

	public void setup() {
		System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver");
		driver = new FirefoxDriver();
		js = (JavascriptExecutor) driver;
		driver.get("https://opensource-demo.orangehrmlive.com/");
		driver.manage().window().maximize();
	}

	@Test
	public void loginByJSExecutor() throws InterruptedException {

		WebElement username = driver.findElement(By.name("txtUsername"));
		WebElement password = driver.findElement(By.name("txtPassword"));
		WebElement login = driver.findElement(By.name("Submit"));

		js.executeScript("window.scrollBy(0,150)");
		Thread.sleep(5000);
		/*
		 * username.sendKeys(uname); password.sendKeys(pass);
		 */
		
		/*
		 * username.sendKeys(uname); password.sendKeys(pass);
		 */
		js.executeScript("arguments[0].value='Admin';", username);
		js.executeScript("arguments[0].value='admin123';", password);
		js.executeScript("arguments[0].click();", login);
		Thread.sleep(5000);
		js.executeScript("alert('Welcome!');");
	}

	@AfterTest
	public void cleanup() {
		driver.quit();
	}
}
