import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class RightClickCopyWithAction {

	public static WebDriver driver;
	public static Actions action;
	public static Alert alert;

	@BeforeClass
	public void setup() {
		System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver");
		driver = new FirefoxDriver();
		action = new Actions(driver);
		driver.get("https://opensource-demo.orangehrmlive.com");
		driver.manage().window().maximize();
	}

	@BeforeMethod
	public void login() {
		WebElement uname = driver.findElement(By.id("txtUsername"));
		uname.sendKeys("Admin");
		WebElement pwd = driver.findElement(By.id("txtPassword"));
		pwd.sendKeys("admin123");
		WebElement login = driver.findElement(By.id("btnLogin"));
		login.click();
	}

	@Test
	public void performRightClick() throws InterruptedException {
		WebElement element = driver.findElement(By.xpath("//*[@id=\"menu_admin_viewAdminModule\"]/b"));
		element.click();
		Thread.sleep(500);
		action.contextClick(element).sendKeys(Keys.DOWN).sendKeys(Keys.ENTER).perform();
		/*
		 * Thread.sleep(500); alert = driver.switchTo().alert(); String actualAlertText
		 * = alert.getText(); System.out.println("Actual alert text : " +
		 * actualAlertText); alert.accept();
		 */
		Thread.sleep(5000);
	}

	@AfterTest
	public void cleanup() {
		driver.quit();
	}

}
